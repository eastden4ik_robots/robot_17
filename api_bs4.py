
from lxml import html
import requests

import api_functions


def api_get_discounts(city: str, store: str, categories: list, date: str) -> [(str, str, str, str, str, str)]:
    discounts = []

    response_cities_id = requests.get('https://skidkaonline.ru/apiv3/cities/')
    cities = response_cities_id.json()
    c_id = str
    for c in cities['cities']:
        if c['slug'] == city:
            c_id = c['id']

    link_discounts = f'https://skidkaonline.ru/apiv3/products/?limit=100' \
                     f'&offset=0&date={date}' \
                     f'&pcategories_ids={",".join(categories)}' \
                     f'&shop_id={store}&city_id={c_id}' \
                     f'&fields=id,name,imagefull,priceafter,date,units'
    print(link_discounts)

    response = requests.get(link_discounts)
    data = response.json()
    print(data)
    for el in data['products']:
        discounts.append((el['id'], el['name'], el['imagefull']['src'], el['priceafter'], el['date'], el['units']))

    print(discounts)
    return discounts


def api_get_store_categories(city: str, store: str) -> [(str, str)]:
    categories = []
    response = requests.get(f'https://skidkaonline.ru/{city}/shops/{store}/')
    soup = html.fromstring(response.text)
    a_elements = soup.xpath('.//*[@class="main-product-cats"]'
                            '//li[contains(@class," show-cat" ) or contains(@class," hide-cat")]'
                            '/span[contains(@class,"menu-grouped")]/a[contains(@class,"cat-link")]')
    for a in a_elements:
        if a.text.strip() in api_functions.api_get_categories():
            data_id = a.attrib['data-id']
            category = a.text.strip()
            categories.append((data_id, category))
    return categories


def api_get_product_stores(city: str) -> [(str, str, str)]:
    stores = []
    response = requests.get(f'https://skidkaonline.ru/{city}/')
    soup = html.fromstring(response.text)
    li_elements = soup.xpath(".//*[@id='shops-carousel']//li")
    for li in li_elements:
        if len(li.findall('h3')) > 0 and li.find('h3').find('a').text == 'Электроника и Бытовая Техника':
            break
        else:
            a = li.xpath('.//div[@class="image"]/a')
            print(a[0].attrib['title'])
            if a[0].attrib['title'] in api_functions.api_get_dict_stores()[city]:
                stores.append(
                    (a[0].attrib['title'],
                     a[0].attrib['href'].split('/shops/')[1].replace('/', ''),
                     li.attrib['data-sid']))
    return stores


def api_get_cities() -> [(str, str)]:
    cities = []
    response = requests.get('https://skidkaonline.ru/moskva/')
    soup = html.fromstring(response.text)
    a_links = soup.xpath(".//*[@id='cities-selector']//ul[@role='menu']/li/a")
    for a in a_links:
        if a.find('span').text in api_functions.api_get_cities():
            cities.append((a.find('span').text, a.attrib['href'].replace('/', '')))
    return cities
