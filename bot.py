
from loguru import logger

from loader import bot, storage


async def on_shutdown(dp):
    await bot.close()
    await storage.close()

if __name__ == '__main__':
    from aiogram import executor
    from handlers.BaseCommandHandler import dispatcher
    from handlers.FilterCommandHandler import dispatcher

    logger.info('Бот со скидками запущен.')
    executor.start_polling(dispatcher, on_shutdown=on_shutdown)
