
from aiogram import types
from loguru import logger

from loader import dispatcher


@dispatcher.message_handler(commands={"start", "restart"})
async def start_handler(event: types.Message):
    logger.info(
        f'Команда start от пользователя [{event.from_user.id} - {event.from_user.full_name}]')
    keyboard = types.ReplyKeyboardMarkup()
    keyboard.add('/discounts - Поехали. Выбор городов.')
    await event.answer(f'Привет, {event.from_user.full_name} 👋!\n'
                       f'Это бот для поиска скидок по сайту skidkaonline.ru.\n'
                       f'Для просмотра доступных комманд и по работе с ботом используй командау /help\n'
                       f'Кнопки Назад сбрасывают соответсвенно выбранные фильтры.\n'
                       f'Поехали!', reply_markup=keyboard)


@dispatcher.message_handler(commands={"help"})
async def help_handler(event: types.Message):
    logger.info(
        f'Команда start от пользователя [{event.from_user.id} - {event.from_user.full_name}]')
    keyboard = types.ReplyKeyboardMarkup()
    keyboard.add('/discounts - Поехали. Выбор городов.')
    await event.answer(
        f'Приветсвую тебя, {event.from_user.full_name} 👋!\n'
        f'Бот для поиска скидок по сайту skidkaonline.ru.\n'
        f'Ты просматриваешь раздел помощи. Не гарантирую,'
        f'что тебе смогу помочь, но хотя бы попытаюсь или пошлю куда подальше!\n\n'
        f'/start, /restart - первоначальный запуск или перезапуск системы получения скидок или '
        f'соответсвующая кнопка в конце всего алгоритма получения кнопок.\n'
        f'/discounts - Начало заполнение фильтра\n'
        f'/help - Раздел помощи. \n\n', reply_markup=keyboard)
