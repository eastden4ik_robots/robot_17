import asyncio
import datetime
import os

import openpyxl
from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Command
from aiogram.types import InputMediaPhoto

import api_bs4
import utils
from loader import dispatcher
from states.SkidkaOnlineFilter import SkidkaOnlineFilter


@dispatcher.message_handler(Command("discounts"), state=None)
async def start_cities_filter(event: types.Message, state: FSMContext):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    cities = api_bs4.api_get_cities()
    for city_ru, city_en in cities:
        keyboard.add(f'г. {city_ru}')
    keyboard.add('Далее. Выбор магазинов.')
    await SkidkaOnlineFilter.Q1_Cities.set()
    async with state.proxy() as data:
        data["cities"] = []
    await event.answer(
        "Выберите город или города. "
        "При отсутствии нужного города впишите в формате: "
        "г. Название города как на сайте",
        reply_markup=keyboard)


@dispatcher.message_handler(state=SkidkaOnlineFilter.Q1_Cities)
async def continue_cities_filter(event: types.Message, state: FSMContext):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    cities = api_bs4.api_get_cities()
    for city_ru, city_en in cities:
        keyboard.add(f'г. {city_ru}')
    keyboard.add('Далее. Выбор магазинов.')
    if event.text.startswith('г. '):
        async with state.proxy() as data:
            data['cities'].append(event.text.split("г. ")[1])
        await event.answer(
            # "Выберите город или города. "
            # "При отсутствии нужного города впишите в формате: "
            # "г. Название города как на сайте",
            reply_markup=keyboard)
        await SkidkaOnlineFilter.Q1_Cities.set()
    elif event.text == 'Далее. Выбор магазинов.' and state.get_data('cities') != []:
        await SkidkaOnlineFilter.Q2_Stores.set()
        async with state.proxy() as data:
            data["stores"] = []
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        city_en = str
        async with state.proxy() as data:
            for city in data['cities']:
                for c_ru, c_en in api_bs4.api_get_cities():
                    if c_ru == city:
                        city_en = c_en
                        break
                stores = api_bs4.api_get_product_stores(city_en)
                for store_ru, store_en, store_id in stores:
                    keyboard.add(f'м. {city} - {store_ru}')
        keyboard.add('Далее. Выбор категорий.')
        await event.answer(
            "Выберите магазин или магазины. При отсутствии нужного магазина впишите в формате: "
            "м. Название магазина как на сайте",
            reply_markup=keyboard)


@dispatcher.message_handler(state=SkidkaOnlineFilter.Q2_Stores)
async def continue_store_filter(event: types.Message, state: FSMContext):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    city_en = str
    async with state.proxy() as data:
        for city in data['cities']:
            for c_ru, c_en in api_bs4.api_get_cities():
                if c_ru == city:
                    city_en = c_en
                    break
            stores = api_bs4.api_get_product_stores(city_en)
            for store_ru, store_en, store_id in stores:
                keyboard.add(f'м. {city} - {store_ru}')
    keyboard.add('Далее. Выбор категорий.')
    if event.text.startswith('м. '):
        async with state.proxy() as data:
            data['stores'].append(event.text.split("м. ")[1])
        await event.answer(
            # "Выберите магазин или магазины. При отсутствии нужного магазина впишите в формате: "
            # "м. Название магазина как на сайте",
            reply_markup=keyboard)
        await SkidkaOnlineFilter.Q2_Stores.set()
    elif event.text == 'Далее. Выбор категорий.' and state.get_state('stores') != []:
        await SkidkaOnlineFilter.Q3_Categories.set()
        async with state.proxy() as data:
            data["categories"] = []
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        categories = api_bs4.api_get_store_categories('moskva', 'magnit')
        for data_id, category in categories:
            keyboard.add(f'к. {category}')
        keyboard.add('Далее. Выбор даты.')
        await event.answer(
            "Выберите категорию или категории. При отсутствии нужной категории впишите в формате: "
            "к. Нащвание категории как на сайте", reply_markup=keyboard)


@dispatcher.message_handler(state=SkidkaOnlineFilter.Q3_Categories)
async def continue_categories_filter(event: types.Message, state: FSMContext):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    categories = api_bs4.api_get_store_categories('moskva', 'magnit')
    for data_id, category in categories:
        keyboard.add(f'к. {category}')
    keyboard.add('Далее. Выбор даты.')
    if event.text.startswith('к. '):
        async with state.proxy() as data:
            data['categories'].append(event.text.split("к. ")[1])
        await event.answer(
            # "Выберите категорию или категории. При отсутствии нужной категории впишите в формате: "
            # "к. Нащвание категории как на сайте",
            reply_markup=keyboard)
        await SkidkaOnlineFilter.Q3_Categories.set()
    elif event.text == 'Далее. Выбор даты.' and state.get_state('categories') != []:
        await SkidkaOnlineFilter.Q4_Dates.set()
        async with state.proxy() as data:
            data["dates"] = []
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        keyboard.add(f'д. {datetime.date.today().strftime("%d-%m-%Y")}')
        keyboard.add(f'д. {(datetime.date.today() + datetime.timedelta(days=1)).strftime("%d-%m-%Y")}')
        keyboard.add('Далее. Получение скидок.')
        await event.answer(
            "Впишите дату в формате 'д. dd-MM-yyyy' или выберите нужную клавишу и затем нажмите 'Далее'.",
            reply_markup=keyboard)


@dispatcher.message_handler(state=SkidkaOnlineFilter.Q4_Dates)
async def continue_dates_filter(event: types.Message, state: FSMContext):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(f'д. {datetime.date.today().strftime("%d-%m-%Y")}')
    keyboard.add(f'д. {(datetime.date.today() + datetime.timedelta(days=1)).strftime("%d-%m-%Y")}')
    keyboard.add('Далее. Получение скидок.')
    if event.text.startswith('д. '):
        async with state.proxy() as data:
            data['dates'].append(event.text.split("д. ")[1])
        await event.answer(
            # "Впишите дату в формате 'д. dd-MM-yyyy' или выберите нужную клавишу и затем нажмите 'Далее'.",
            reply_markup=keyboard)
        await SkidkaOnlineFilter.Q4_Dates.set()
    elif event.text == 'Далее. Получение скидок.' and state.get_state('dates') != []:
        await SkidkaOnlineFilter.Q5_AnswerType.set()
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        keyboard.add('Напрямую в телеграмм')
        keyboard.add('В excel файле')
        await event.answer('В каком виде отправить данные?', reply_markup=keyboard)


@dispatcher.message_handler(state=SkidkaOnlineFilter.Q5_AnswerType)
async def continue_type_answer_filter(event: types.Message, state: FSMContext):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if event.text == 'Напрямую в телеграмм':
        keyboard.add('/discounts - Начать заново!')

        msg = await event.answer('В процессе...')
        process = 0.0
        await msg.edit_text(f'Завершено {process} %')
        async with state.proxy() as data:
            stores_list_len = float(len(data['stores']))
            for store in data['stores']:
                process += 1.0

                for date in data['dates']:
                    result_files = utils.get_result_files(
                        store=store,
                        user_categories=data['categories'],
                        user_dates=[date])

                    await event.answer(f'Лови скидки по сети [{store}] на дату {date}!', reply_markup=keyboard)

                    media = []
                    for img_id, name, src, \
                        price, date, units in result_files:
                        media.append(InputMediaPhoto(src,
                                                     (f"Цена со скидкой {price} {units}"
                                                      if price != "" else "Ценник на картинке)") +
                                                     f"\nДаты скидок: {date}"))  # name -> ''

                    grouped_list = utils.chunk_using_generators(media, 10)
                    for lst in grouped_list:
                        await event.answer_media_group(lst)
                    await asyncio.sleep(5)

                await msg.edit_text(f'Завершено {((process / stores_list_len) * 100.0) - 1.0:.2f} %')
                await asyncio.sleep(2)

        await asyncio.sleep(2)
        await msg.edit_text(f'Завершено 100 %')
        await event.answer('Готово!', reply_markup=keyboard)
        await state.finish()
    elif event.text == 'В excel файле':
        keyboard.add('/discounts - Начать заново!')

        work_book = openpyxl.Workbook()

        msg = await event.answer('В процессе...')
        process = 0.0
        await msg.edit_text(f'Завершено {process} %')
        async with state.proxy() as data:
            stores_list_len = float(len(data['stores']))
            for store in data['stores']:
                process += 1.0

                result_files = utils.get_result_files(
                    store=store,
                    user_categories=data['categories'],
                    user_dates=data['dates'])

                work_book.create_sheet(title=f"Store_{store}"[:31])

                work_book[f"Store_{store}"[:31]]['A1'].value = 'Product ID'
                work_book[f"Store_{store}"[:31]]['B1'].value = 'Product Name'
                work_book[f"Store_{store}"[:31]]['C1'].value = 'Product Image'
                i = 2
                for img_id, name, src in result_files:
                    work_book[f"Store_{store}"[:31]][f'A{i}'].value = img_id
                    work_book[f"Store_{store}"[:31]][f'B{i}'].value = name
                    work_book[f"Store_{store}"[:31]][f'C{i}'].value = src
                    i += 1

                await msg.edit_text(f'Завершено {((process / stores_list_len) * 100.0) - 1.0:.2f} %')
                await asyncio.sleep(2)

        excel_file_name = f'Result_{datetime.datetime.now().strftime("%d-%m-%Y")}_{event.from_user.full_name}.xlsx'

        work_book.remove(work_book['Sheet'])
        work_book.save(excel_file_name)
        with open(excel_file_name, 'rb') as excel:
            await event.answer_document(excel, caption='Лови файл! 😺')

        await asyncio.sleep(2)
        await msg.edit_text(f'Завершено 100 %')
        await event.answer('Готово!', reply_markup=keyboard)
        os.remove(excel_file_name)
        await state.finish()
