import datetime
import os
import time
import urllib.request

import utils
from pages.SkidkaOnlinePage import SkidkaOnlinePage


def api_get_stores() -> list:
    stores = ['"Магнит у дома"', 'Пятерочка', 'EUROSPAR Express',
              'Ашан', 'Билла', 'Верный',
              'Виктория', 'Дикси', 'Зельгрос',
              'Карусель', 'Лента гипермаркет', 'Магнолия',
              'Метро', 'Окей гипермаркет', 'Перекресток', 'Монетка дискаунтер', 'Мария-Ра']
    return stores


def api_get_dict_stores() -> dict:
    return {
        'moskva': ['"Магнит у дома"', '"Пятерочка"', 'Пятерочка', 'EUROSPAR Express',
                   'Ашан', 'Билла', 'Верный',
                   'Виктория', 'Дикси', 'Зельгрос',
                   'Карусель', 'Лента гипермаркет', 'Магнолия',
                   'Метро', 'Окей гипермаркет', 'Перекресток'],
        'yekaterinburg': ['Монетка дискаунтер'],
        'novosibirsk': ['Мария-Ра']
    }


def api_get_cities() -> list:
    cities = ['Москва', 'Екатеринбург', 'Новосибирск']
    return cities


def api_get_categories() -> list:
    categories = ['Корм для кошек и котов', 'Животные и товары для питомцев']
    return categories


def get_discounts(
        user_id: int,
        cities: list,
        stores: list,
        categories: list,
        dates: list,
        to_download: bool) -> (str, list):
    browser = utils.get_browser(user_id)
    page = SkidkaOnlinePage(browser)

    all_images = []
    screenshots_path = f"{os.getenv('screenshots')}/{datetime.date.today()}"
    utils.create_if_necessary(os.getenv('screenshots'))
    utils.create_if_necessary(screenshots_path)
    utils.create_if_necessary('logs')
    result_folder = os.getenv('results.folder')
    utils.create_if_necessary(result_folder)
    run_folder = f'{result_folder}/Запуск с номером {utils.get_name_for_images()} для пользователя {user_id}'
    utils.create_if_necessary(run_folder)

    for city in cities:
        city_folder = run_folder + "/" + city
        utils.create_if_necessary(city_folder)
        for store in stores:
            store_folder = city_folder + "/" + store
            utils.create_if_necessary(store_folder)

            page.open_web_site()
            time.sleep(5)
            page.select_city(city)
            page.screenshot(screenshots_path, 'City_chosen')
            page.select_store(store)
            page.screenshot(screenshots_path, 'Store_chosen')
            page.scroll_to_filter()

            for category in categories:
                page.select_category(category)

            page.screenshot(screenshots_path, 'Filters_chosen')
            for date_to_find in dates:
                date_folder = f'{store_folder}/Скидки на {date_to_find} дату'
                utils.create_if_necessary(date_folder)
                day, month, year = utils.convert_date(date_to_find)
                page.set_date(day=day, month=month, year=year)
                time.sleep(5)
                page.screenshot(screenshots_path, 'Date_chosen_and_get_discounts')
                images = page.get_images()
                for image in images:
                    all_images.append(image)
                    if to_download:
                        urllib.request.urlretrieve(
                            image,
                            f'{date_folder}/SKIDKI_IMG_{utils.get_name_for_images()}.JPG')

    browser.close()
    return run_folder, all_images
