import calendar
import os
from datetime import datetime

from loguru import logger
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.firefox.options import Options

import api_bs4


def create_if_necessary(folder: str):
    if not os.path.exists(folder):
        os.mkdir(folder)


def convert_date(date: str) -> (int, str, int):
    day = datetime.fromisoformat(date).day
    month = datetime.fromisoformat(date).month
    year = datetime.fromisoformat(date).year
    return day, str(calendar.month_name[month]), year


def get_browser(user_id: int) -> webdriver:

    if os.getenv('browser') == 'chrome':
        path_to_extension = os.getenv('ad.block.1')
        chrome_options = Options()
        chrome_options.add_extension(path_to_extension)
        chrome_options.add_argument("--disable-notifications")
        chrome_options.add_argument(f"--user-data-dir={os.getenv('chrome.profile')}/{user_id}")
        chrome_options.page_load_strategy = 'normal'
        driver = webdriver.Chrome(
            executable_path=os.getenv('CHROMEDRIVER_PATH'),
            options=chrome_options
        )
    elif os.getenv('browser') == 'firefox':
        path_to_extension = os.getenv('ad.block.2')
        firefox_options = Options()
        firefox_options.add_argument("--disable-notifications")
        firefox_options.add_argument('--headless')

        driver = webdriver.Firefox(
            executable_path=os.environ.get('GECKODRIVER_PATH'),
            options=firefox_options
        )
        driver.install_addon(path_to_extension, temporary=True)
    else:
        return None

    driver.maximize_window()
    return driver


def get_name_for_images() -> str:
    return str(str(datetime.today().date().day) + "." +
               str(datetime.today().date().month) + "." +
               str(datetime.today().date().year) + "_" +
               str(datetime.today().time().hour) + "." +
               str(datetime.today().time().minute) + "." +
               str(datetime.today().time().second) + "." +
               str(datetime.today().time().microsecond))


def get_result_files(store: str, user_categories: list, user_dates: list) -> [(str, str, str, str, str, str)]:
    city_ru, store_ru = store.split(' - ')
    city_en = str
    store_id = str
    for c_ru, c_en in api_bs4.api_get_cities():
        if c_ru == city_ru:
            city_en = c_en
            break

    for s_ru, s_en, s_id in api_bs4.api_get_product_stores(city_en):
        if s_ru == store_ru:
            store_id = s_id
            break
    categories = []
    for data_id, cat in api_bs4.api_get_store_categories('moskva', 'magnit'):
        if cat in user_categories:
            categories.append(data_id)
    logger.info(categories)
    result_files = []
    for date in user_dates:
        result_files += api_bs4.api_get_discounts(city_en, store_id, categories, date)
    return result_files


def chunk_using_generators(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i:i + n]
