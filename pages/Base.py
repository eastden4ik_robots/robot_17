
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

import os


class BasePage:

    def __init__(self, driver):
        self.driver = driver
        self.base_url = os.getenv('url')

    def element_exists(self, locator, time=10) -> bool:
        if self.find_element(locator, time).is_displayed():
            return True
        else:
            return False

    def find_element(self, locator, time=10):
        return WebDriverWait(self.driver, time).until(ec.presence_of_element_located(locator),
                                                      message=f"Can't find element by locator {locator}")

    def find_elements(self, locator, time=10):
        return WebDriverWait(self.driver, time).until(ec.presence_of_all_elements_located(locator),
                                                      message=f"Can't find elements by locator {locator}")

    def execute_script(self, script: str):
        self.driver.execute_script(script=script)

    def go_to_site(self):
        return self.driver.get(self.base_url)
