
import time

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from lxml import html

import utils
from .Base import BasePage

from loguru import logger


class SkidkaOnlineLocators:

    XPATH_BTN_CITIES_FILTER = (By.XPATH, ".//*[contains(@id,'cities-selector')]//a[contains(@class,'dropdown-toggle')]")
    XPATH_INPUT_CITIES_FILTER = (By.XPATH, ".//*[contains(@id,'cities-selector')]//input[contains(@placeholder,"
                                           "'Название города')]")

    XPATH_STORES_INPUT = (By.XPATH, ".//input[contains(@placeholder,'Поиск по категориям, магазинам')]")
    XPATH_A_CATS_MORE_LESS = (By.XPATH, ".//a[contains(@class,'more-less-cats')]/span[contains(text(),'expand_more')]")

    XPATH_CATEGORIES_TITLE = (By.XPATH, ".//h1[contains(text(),'Текущие и будущие акции')]")
    XPATH_NEW_DISCOUNTS_TITLE = (By.XPATH, ".//h2[contains(text(),'Уведомления на новые акции')]")

    XPATH_DATE_PICKER = (By.XPATH, ".//a[contains(@class,'date-selector-link')]")
    XPATH_DATE_PICKER_MONTH_YEAR = (By.XPATH, ".//div[contains(@class,'datepicker')]"
                                              "/div[contains(@class,'datepicker-days')]"
                                              "//thead//th[@class='datepicker-switch']")
    XPATH_DATE_PICKER_YEAR = (By.XPATH, ".//div[contains(@class,'datepicker')]"
                                        "/div[contains(@class,'datepicker-months')]"
                                        "//thead//th[@class='datepicker-switch']")

    XPATH_ITEMS_SKIDKI = (By.XPATH, ".//div[contains(@class,'discounts ')]")
    XPATH_ITEMS_MORE_DISCOUNTS = (By.XPATH, ".//a[contains(@class,'more-discounts-btn')]")
    XPATH_ITEMS_IMG_SKIDKI = (By.XPATH, ".//div[contains(@class,'discounts ')]//a[@class='image-link']/img")

    ADS_TAG_NAME = (By.TAG_NAME, "iframe")
    ADS_TAG_NAME_2 = (By.XPATH, ".//iframe[@id='aswift_22']")
    BODY_TAG_NAME = (By.TAG_NAME, "body")

    @staticmethod
    def XPATH_DROPDOWN_CITIES(city: str) -> (By.XPATH, str):
        return By.XPATH, f".//*[@id='cities-selector']//*[@class='dropdown-menu open']" \
                         f"//a/span[@class='text' and text()='{city}']"

    @staticmethod
    def XPATH_SELECT_DAY_DATE_PICKER_DAYS(day: int) -> (By.XPATH, str):
        return By.XPATH, f".//div[contains(@class,'datepicker')]/div[contains(@class,'datepicker-days')]" \
                         f"//tbody//td[(@class='day' or @class='today day') and text()='{day}']"

    @staticmethod
    def XPATH_SELECT_MONTH_DATE_PICKER_MONTHS(month: str) -> (By.XPATH, str):
        return By.XPATH, f".//div[contains(@class,'datepicker')]/div[contains(@class,'datepicker-months')]" \
                         f"//tbody//td/span[text()='{month}' or text()='{month.title()}']"

    @staticmethod
    def XPATH_SELECT_YEAR_DATE_PICKER_YEARS(year: int) -> (By.XPATH, str):
        return By.XPATH, f".//div[contains(@class,'datepicker')]/div[contains(@class,'datepicker-years')]" \
                         f"//tbody//td/span[text()='{year}']"

    @staticmethod
    def XPATH_SELECT_CATEGORY(category: str) -> (By.XPATH, str):
        return By.XPATH, f".//ul[contains(@class,'cats-list')]/li[contains(@class,'show-cat') or " \
                         f"contains(@class,'hide-cat')]" \
                         f"//a[contains(text(),'{category}')]"

    @staticmethod
    def XPATH_DROPDOWN_STORES(store: str) -> (By.XPATH, str):
        return By.XPATH, f".//input[contains(@placeholder,'Поиск по категориям, магазинам')]" \
                         f"//following-sibling::div//a/span[contains(text(),'{store}')]"

    @staticmethod
    def XPATH_SELECT_OPTION_CITIES(city: str) -> (By.XPATH, str):
        return By.XPATH, f".//*[contains(@id,'cities-selector')]//ul/li//span[contains(text(),'{city}')]"

    @staticmethod
    def XPATH_SELECT_STORE_BY_NAME(store: str) -> (By.XPATH, str):
        return By.XPATH, f".//li//a[contains(@title,'{store}')]"

    @staticmethod
    def XPATH_SELECTED_STORE_BY_NAME(store: str) -> (By.XPATH, str):
        return By.XPATH, f".//h1[contains(text(),'{store}')]"


class SkidkaOnlinePage(BasePage):

    def screenshot(self, path: str, element: str = ''):
        self.driver.save_screenshot(f'{path}/Screen_{element}_{utils.get_name_for_images()}.png')

    def open_web_site(self):
        self.go_to_site()

    def get_store_names(self) -> list:
        page_source = self.driver.page_source
        soup = html.fromstring(page_source)
        a_links = soup.xpath(".//*[@id='shops-carousel']//*[@class='image']//a/@title")
        for link in a_links:
            print(link)
        return a_links

    def check_offset(self):
        amount = self.find_element(SkidkaOnlineLocators.XPATH_ITEMS_SKIDKI, 30).get_attribute('data-offset')
        logger.info('Initial offset is {}', amount)
        while int(amount) % 30 == 0:
            time.sleep(1)
            self.button_click_using_js(SkidkaOnlineLocators.XPATH_ITEMS_MORE_DISCOUNTS)
            amount = self.find_element(SkidkaOnlineLocators.XPATH_ITEMS_SKIDKI, 30).get_attribute('data-offset')
            logger.info('Offset is {}', amount)

    def get_images(self) -> list:
        images = self.find_elements(SkidkaOnlineLocators.XPATH_ITEMS_IMG_SKIDKI, 30)
        returned_images = []
        for i, image in enumerate(images):
            logger.info(f"{i + 1} - src -> {image.get_attribute('src')}")
            returned_images.append(image.get_attribute('src'))
        return returned_images

    def set_date(self, day: int, month: str, year: int):
        self.button_click_using_js(SkidkaOnlineLocators.XPATH_DATE_PICKER)
        if self.element_exists(SkidkaOnlineLocators.XPATH_DATE_PICKER_MONTH_YEAR, 30):
            correct_month = False
            correct_year = False
            tmp_value = self.find_element(SkidkaOnlineLocators.XPATH_DATE_PICKER_MONTH_YEAR).text
            logger.info(f"Choose {day}-{month}-{year}")
            logger.info(f"Temp value {tmp_value}")
            if month in tmp_value:
                correct_month = True
            if str(year) in tmp_value:
                correct_year = True
            logger.info(f"is_month_correct {correct_month}, is_year_correct {correct_year}")
            if not correct_year:
                self.button_click_using_js(SkidkaOnlineLocators.XPATH_DATE_PICKER_MONTH_YEAR)
                self.button_click_using_js(SkidkaOnlineLocators.XPATH_DATE_PICKER_YEAR)
                self.button_click_using_js(SkidkaOnlineLocators.XPATH_SELECT_YEAR_DATE_PICKER_YEARS(year=year))
                self.button_click_using_js(SkidkaOnlineLocators.XPATH_SELECT_MONTH_DATE_PICKER_MONTHS(month=month[:3]))
            if not correct_month:
                self.button_click_using_js(SkidkaOnlineLocators.XPATH_DATE_PICKER_MONTH_YEAR)
                self.button_click_using_js(SkidkaOnlineLocators.XPATH_SELECT_MONTH_DATE_PICKER_MONTHS(month=month[:3]))
            self.button_click_using_js(SkidkaOnlineLocators.XPATH_SELECT_DAY_DATE_PICKER_DAYS(day=day))
            logger.info(f"Chosen date {day}-{month}-{year}")

    def scroll_to_filter(self):
        if self.element_exists(SkidkaOnlineLocators.XPATH_A_CATS_MORE_LESS, 30):
            self.scroll_into_view_js(SkidkaOnlineLocators.XPATH_CATEGORIES_TITLE)
            self.button_click_using_js(SkidkaOnlineLocators.XPATH_A_CATS_MORE_LESS)

    def select_category(self, category: str):
        if self.element_exists(SkidkaOnlineLocators.XPATH_SELECT_CATEGORY(category), 30):
            self.button_click_using_js(SkidkaOnlineLocators.XPATH_SELECT_CATEGORY(category))
            logger.info(f"Category {category} was chosen.")

    def select_store(self, store: str):
        time.sleep(5)
        if self.element_exists(SkidkaOnlineLocators.XPATH_STORES_INPUT, 30):
            self.button_click_using_js(SkidkaOnlineLocators.XPATH_STORES_INPUT)
            self.find_element(SkidkaOnlineLocators.XPATH_STORES_INPUT).send_keys(store)
            # self.set_value_using_js(SkidkaOnlineLocators.XPATH_STORES_INPUT, store)

            # self.send_enter()
            if self.element_exists(SkidkaOnlineLocators.XPATH_DROPDOWN_STORES(store), 30):
                self.find_element(SkidkaOnlineLocators.XPATH_DROPDOWN_STORES(store), 10).click()
                if self.element_exists(SkidkaOnlineLocators.XPATH_SELECTED_STORE_BY_NAME(store), 30):
                    logger.info(f"Store: {store} opened!")
                else:
                    logger.info(f"Store: {store} not opened!")

    def select_city(self, city: str):
        cities_select = self.find_element(SkidkaOnlineLocators.XPATH_BTN_CITIES_FILTER, 5)
        cities_select.click()
        cities_input = self.find_element(SkidkaOnlineLocators.XPATH_INPUT_CITIES_FILTER, 5)
        cities_input.send_keys(city)
        time.sleep(5)
        self.button_click_using_js(SkidkaOnlineLocators.XPATH_DROPDOWN_CITIES(city))
        logger.info(f"City: {city} opened!")

    def send_esc(self):
        self.find_element(SkidkaOnlineLocators.BODY_TAG_NAME, 10).send_keys(Keys.ESCAPE)

    def send_enter(self):
        self.find_element(SkidkaOnlineLocators.BODY_TAG_NAME, 10).send_keys(Keys.ENTER)

    def send_end(self):
        self.find_element(SkidkaOnlineLocators.BODY_TAG_NAME, 10).send_keys(Keys.END)

    def remove_alert(self):
        self.driver.execute_script("window.onbeforeunload = function() {};")

    def button_click_using_js(self, button_xpath: (By.XPATH, str)):
        button = self.find_element(button_xpath, 30)
        self.driver.execute_script("arguments[0].click();", button)

    def set_value_using_js(self, input_xpath: (By.XPATH, str), value: str):
        input_js = self.find_element(input_xpath, 30)
        self.driver.execute_script(f"arguments[0].setAttribute('value', '{value}')", input_js)

    def scroll_into_view_js(self, element_xpath: (By.XPATH, str)):
        element = self.find_element(element_xpath)
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
