
import os
import sys

from dotenv import load_dotenv
from loguru import logger

from api_functions import get_discounts

load_dotenv()
logger.add(sys.stderr, format="{time} {level} {message}", filter="my_module", level="INFO")
logger.add(os.getenv("logs"))
logger.level("INFO")


def main_program():

    logger.info(f'Starting program in browser {os.getenv("browser")}')
    logger.info(f'Драйвер для браузера: {os.getenv("FIREFOXDRIVER_PATH")}')

    cities = ['Москва']  # , 'Москва'
    stores = ['Пятерочка', 'Дикси']  # 'Дикси', 'Магнит Семейный', 'Пятерочка'
    categories = ['Корм для кошек и котов', 'Животные и товары для питомцев']
    dates = ['2021-04-19', '2021-04-16']  # , '2021-04-13', '2021-04-16', '2021-04-19'

    get_discounts(user_id=1, cities=cities, stores=stores, categories=categories, dates=dates, to_download=True)


if __name__ == '__main__':
    main_program()
