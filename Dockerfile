FROM python:3.9-slim-buster

RUN apt-get update && apt-get install -y \
    build-essential \
    libpq-dev \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

RUN python3.9 -m pip install --no-cache-dir --upgrade \
    pip \
    setuptools \
    wheel
COPY requirements.txt /usr/src/app/
RUN python3.9 -m pip install --no-cache-dir \
    -r requirements.txt
COPY . /usr/src/app


CMD ["python3.9", "bot.py"]
