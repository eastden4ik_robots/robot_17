from aiogram.dispatcher.filters.state import StatesGroup, State


class SkidkaOnlineFilter(StatesGroup):
    Q1_Cities = State()  # Choosing cities
    Q2_Stores = State()  # Choosing stores
    Q3_Categories = State()  # Choosing categories
    Q4_Dates = State()  # Choosing dates
    Q5_AnswerType = State()  # Choosing type of the answer
