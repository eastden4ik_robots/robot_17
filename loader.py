
import os
import sys

from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from dotenv import load_dotenv
from loguru import logger

load_dotenv()
logger.add(sys.stderr, format="{time} {level} {message}", filter="my_module", level="INFO")
logger.add(os.getenv("logs"))
logger.level("INFO")

bot = Bot(token=os.getenv('BOT_TOKEN'))
storage = MemoryStorage()
dispatcher = Dispatcher(bot=bot, storage=storage)
